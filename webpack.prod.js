const webpack = require("webpack");
const path = require("path");
const merge = require("webpack-merge");

module.exports = merge(require("./webpack.config")("production"), {
    mode: "production",
    devtool: "source-map",
    output: {
        filename: "scripts/demo.[name].[hash].bundle.js",
        sourceMapFilename: "scripts/demo.[name].[chunkhash].bundle.map",
        chunkFilename: "scripts/demo.[id].[chunkhash].chunk.js",
        path: path.resolve(__dirname, "./build/dist")
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true
        })
    ]
});
