import { ConnectedRouter } from "connected-react-router";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Redirect, Route, Switch } from "react-router";
import { PersistGate } from "redux-persist/integration/react";
import "./assets/scss/main.scss";
import configureStore from "./boot/configureStore";
import PercentageComponent from "./screens/percentage/component";
import ShareHolderComponent from "./screens/share-holder/component";

const store = configureStore.setup();

ReactDOM.render(
  <Provider store={store.store}>
    <PersistGate loading={null} persistor={store.persistor}>
      <ConnectedRouter history={configureStore.history}>
        <Switch>
          <Redirect from="/" exact to="/page1" />
          <Route path="/page1" component={ShareHolderComponent} />
          <Route path="/page2" component={PercentageComponent} />
        </Switch>
      </ConnectedRouter>
    </PersistGate>
  </Provider>,
  document.getElementById("getground-root")
);
