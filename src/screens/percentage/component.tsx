import { RootState } from "../../boot/rootState";
import ButtonComponent from "../../containers/components/layout/button";
import ContainerComponent from "../../containers/components/layout/container";
import ShareHolderItemPercentComponent from "../../containers/components/shareholder/item";
import { updateShareHolder } from "../../containers/redux/share-holder/actions";
import React from "react";
import { useHistory } from "react-router-dom";
import { ShareHolder } from "containers/redux/share-holder/state";
import { useSelector, useDispatch } from "react-redux";

const PercentageComponent = (): any => {
  const history = useHistory();
  const { shareHolders } = useSelector((state: RootState) => ({
    shareHolders: state.shareHolders.shareHolders
  }));
  const dispatch = useDispatch();

  const getIdxOfErrorHolderPercent = () => {
    let idx = -1;
    let percent = 0;
    for (let i = 0; i < shareHolders.length; i++) {
      percent = percent + shareHolders[i].percentHolder;
      if (percent > 100) {
        idx = i;
        break;
      }
    }
    return idx !== -1 ? shareHolders[idx].id : null;
  };

  const changePercentHolder = (sharer: ShareHolder, percent: number) => {
    if (percent >= 0) {
      let newHolder = sharer;
      newHolder.percentHolder = percent;
      dispatch(updateShareHolder(newHolder));
    }
  };

  const changePositionHolder = (sharer: ShareHolder, value: string) => {
    let newHolder = sharer;
    if (value === "YES") {
      newHolder.isDirector = true;
    } else {
      newHolder.isDirector = false;
    }
    dispatch(updateShareHolder(newHolder));
  };

  return (
    <>
      <ContainerComponent>
        <div className="page-content page-percentages">
          <p className="page-title">Shareholder details</p>
          <div className="alert gradient-blue">
            <p className="text">
              Now it is time to divide company shares between your shareholders and specify which shareholders are
              directors
            </p>
          </div>

          <div className="box-form form-infor-details mb-35">
            <div className="infor-header">
              <div className="col-first">
                <p className="title">Shareholders</p>
              </div>
              <div className="col-last">
                <p className="title">Shares %</p>
              </div>
              <div className="col-last">
                <p className="title">Director</p>
              </div>
            </div>
            <div className="infor-details">
              {shareHolders.map((sharer, index) => (
                <ShareHolderItemPercentComponent
                  isDirector={sharer.isDirector}
                  sharer={sharer}
                  key={index.toString()}
                  onChangePercentValue={(percent: number) => {
                    changePercentHolder(sharer, percent);
                  }}
                  onChangePositionValue={(position: string) => {
                    changePositionHolder(sharer, position);
                  }}
                  errPercentId={getIdxOfErrorHolderPercent()}
                />
              ))}
            </div>
          </div>
          {shareHolders.length < 8 && (
            <div className="form-group flex-row-center mb-35">
              <ButtonComponent
                className="btn btn-gray btn-add"
                title=""
                onClick={() => {
                  history.replace("/page1");
                }}
              >
                <i className="fa fa-plus"></i> Add shareholder
              </ButtonComponent>
            </div>
          )}
        </div>
      </ContainerComponent>
      <footer className="footer">
        <div className="container  flex-row-end">
          <ButtonComponent
            className="btn btn-outline btn-primary mr-20"
            title="Back"
            onClick={() => {
              history.replace("/page1");
            }}
          />
          <ButtonComponent
            className="btn btn-gradient"
            title="Next"
            onClick={() => {
              history.replace("/page2");
            }}
          />
        </div>
      </footer>
    </>
  );
};

export default PercentageComponent;
