import React from "react";
import { ReactWrapper, mount } from "enzyme";
import ShareHolderComponent from "../component";

describe("ShareHolderComponent", () => {
  let page: ReactWrapper;
  beforeEach(() => {
    page = mount(<ShareHolderComponent />);
  });

  it("should match snap shot", () => {
    expect(page).toMatchSnapshot();
  });

  it("change holder percent", () => {
    page
      .find("ShareHolderItemPercentComponent")
      .first()
      .find("input")
      .first()
      .simulate("change", {
        target: {
          value: "120"
        }
      });
    setTimeout(() => {
      expect(
        page
          .find("ShareHolderItemPercentComponent")
          .first()
          .contains(
            <p className="text-error">
              In total shareholders can only have a percentage that adds up to 100%, please review these details
            </p>
          )
      ).toEqual(false);
    }, 300);
  });

  it("change holder position", () => {
    page
      .find("ShareHolderItemPercentComponent")
      .first()
      .find("button")
      .simulate("click");
    expect(
      page
        .find("ShareHolderItemPercentComponent")
        .first()
        .find("button")
        .text()
    ).toEqual("NO");
  });
});
