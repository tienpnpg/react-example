import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ContainerComponent from "../../containers/components/layout/container";
import ShareHolderEditComponent from "../../containers/components/shareholder/edit";
import { addShareHolders } from "../../containers/redux/share-holder/actions";
import { ShareHolder } from "../../containers/redux/share-holder/state";
import { RootState } from "boot/rootState";
import ButtonComponent from "../../containers/components/layout/button";
import { useHistory } from "react-router-dom";
import { validateEmail } from "../../containers/utils";

const initHolderData: ShareHolder = {
  id: 1,
  firstName: "",
  lastName: "",
  email: "",
  percentHolder: 0,
  isDirector: false
};

const ShareHolderComponent = () => {
  const history = useHistory();
  const { shareHolders } = useSelector((state: RootState) => ({
    shareHolders: state.shareHolders.shareHolders
  }));
  const dispatch = useDispatch();

  const [shareHolderList, setShareHolderList] = useState<ShareHolder[]>(shareHolders);

  useEffect(() => {
    const checkShareHoldersData = (data: ShareHolder[]) => {
      let newData = [...data];
      if (newData.length === 1 && newData[0].id === 0) {
        newData.push(initHolderData);
      }
      setShareHolderList([...newData]);
    };
    checkShareHoldersData(shareHolders);
  }, []);

  const addNewShareHolder = (sharer: ShareHolder) => {
    const newShareHolderList: ShareHolder[] = shareHolderList;
    newShareHolderList.push({ ...initHolderData, id: sharer.id + 1 });
    setShareHolderList([...newShareHolderList]);
  };

  const updateShareHolder = (sharer: ShareHolder, index: number) => {
    const newShareHolderList: ShareHolder[] = shareHolderList;
    newShareHolderList[index] = { ...sharer };
    setShareHolderList([...newShareHolderList]);
  };

  const validShareHolderList = () => {
    let validShareList = true;
    for (let i = 0; i < shareHolderList.length; i++) {
      const sharer = shareHolderList[i];
      if (sharer.email.length && sharer.firstName.length && validateEmail(sharer.email)) {
        validShareList = true;
      } else {
        validShareList = false;
        break;
      }
    }
    if (validShareList) {
      dispatch(addShareHolders(shareHolderList));
      history.replace("/page2");
    } else {
      alert("Please complete info of ShareHolders");
    }
  };

  return (
    <>
      <ContainerComponent>
        <div className="page-content">
          <p className="page-title">Shareholder details</p>
          <div className="alert gradient-blue">
            <p className="text">
              You can have a maximum of 8 share holders (including you). All shareholders need to be people not
              companies.
            </p>
            <p className="text">
              Please note that each of your share holders will need to create an account on getGround. They must also be
              over 18.
            </p>
          </div>
          {shareHolderList.map((sharer, index) => (
            <div key={index.toString()}>
              {sharer.id !== 0 && (
                <ShareHolderEditComponent
                  onUpdateSharer={sharer => {
                    updateShareHolder(sharer, index);
                  }}
                  onAddNewSharer={() => {
                    addNewShareHolder(sharer);
                  }}
                  sharer={sharer}
                  data-id={`sharer-${sharer.id}`}
                  lastSharer={shareHolderList.length === index + 1 && shareHolderList.length < 8}
                />
              )}
            </div>
          ))}
        </div>
      </ContainerComponent>
      <footer className="footer">
        <div className="container  flex-row-end">
          <ButtonComponent
            className="btn btn-outline btn-primary mr-20"
            title="Back"
            onClick={() => {
              history.replace("/page1");
            }}
          />
          <ButtonComponent
            data-id="footer-next-btn"
            className="btn btn-gradient"
            title="Next"
            onClick={() => {
              validShareHolderList();
            }}
          />
        </div>
      </footer>
    </>
  );
};

export default ShareHolderComponent;
