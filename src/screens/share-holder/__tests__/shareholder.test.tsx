import React from "react";
import { ReactWrapper, mount } from "enzyme";
import ShareHolderComponent from "../component";

describe("ShareHolderComponent", () => {
  let page: ReactWrapper;
  beforeEach(() => {
    page = mount(<ShareHolderComponent />);
  });

  it("should match snap shot", () => {
    expect(page).toMatchSnapshot();
  });

  it("Change error value", () => {
    page
      .find("InputComponent")
      .first()
      .find("input")
      .simulate("change", {
        target: {
          value: "12312"
        }
      });
    page
      .find("InputComponent")
      .first()
      .find("input")
      .simulate("change", {
        target: {
          value: ""
        }
      });
    expect(
      page
        .find("InputComponent")
        .first()
        .contains(<small className="error">Invalid</small>)
    ).toEqual(true);
  });

  it("fill value of sharer", () => {
    page
      .find("ShareHolderEditComponent")
      .first()
      .find("InputComponent[data-id='firstName.input']")
      .find("input")
      .simulate("change", {
        target: {
          value: "454564"
        }
      });
    page
      .find("ShareHolderEditComponent")
      .first()
      .find("InputComponent[data-id='lastName.input']")
      .find("input")
      .simulate("change", {
        target: {
          value: "454564"
        }
      });
    page
      .find("ShareHolderEditComponent")
      .first()
      .find("InputComponent[data-id='email.input']")
      .find("input")
      .simulate("change", {
        target: {
          value: "454564@gmail.com"
        }
      });
    page
      .find("ShareHolderEditComponent")
      .first()
      .find("ButtonComponent")
      .find("button")
      .simulate("click");
    expect(page.find("ShareHolderEditComponent")).toHaveLength(2);
  });

  it("Error field in last sharer", () => {
    page
      .find("ShareHolderEditComponent")
      .first()
      .find("InputComponent[data-id='firstName.input']")
      .find("input")
      .simulate("change", {
        target: {
          value: "454564"
        }
      });
    page
      .find("ShareHolderEditComponent")
      .first()
      .find("InputComponent[data-id='lastName.input']")
      .find("input")
      .simulate("change", {
        target: {
          value: "454564"
        }
      });
    page
      .find("ShareHolderEditComponent")
      .first()
      .find("InputComponent[data-id='email.input']")
      .find("input")
      .simulate("change", {
        target: {
          value: "454564@gmai"
        }
      });
    page
      .find("footer")
      .find("ButtonComponent[data-id='footer-next-btn']")
      .find("button")
      .simulate("click");
    expect(page.find("ShareHolderEditComponent")).toHaveLength(1);
  });
});
