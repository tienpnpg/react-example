import { handleActions } from "redux-actions";
import { addShareHolders, updateShareHolder } from "./actions";
import IShareHoldersActionState, { ShareHolder } from "./state";

export default handleActions<any>(
  {
    [addShareHolders.toString()]: (state, action) => {
      const newState: IShareHoldersActionState = state;
      newState.shareHolders = action.payload;
      return { ...newState };
    },
    [updateShareHolder.toString()]: (state, action) => {
      const newState: IShareHoldersActionState = { ...state };
      const holderUpdate: ShareHolder = action.payload;
      const idxHolder = newState.shareHolders.findIndex(holder => holder.id === holderUpdate.id);
      newState.shareHolders[idxHolder] = holderUpdate;
      return { ...newState };
    }
  },
  {
    shareHolders: [{ firstName: "YOU", lastName: "", email: "mememememe@gmail.com", id: 0, percentHolder: 100, isDirector: true }]
  }
);
