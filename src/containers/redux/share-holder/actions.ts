import { createActions } from "redux-actions";
import { ShareHolder } from "./state";

const actions = createActions({
  ADD_SHARE_HOLDERS: (shareHolders: ShareHolder[]) => shareHolders,
  UPDATE_SHARE_HOLDER: (shareHolder: ShareHolder) => shareHolder
});

export const { addShareHolders, updateShareHolder } = actions;
