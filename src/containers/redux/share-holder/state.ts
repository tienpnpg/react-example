export default interface IShareHoldersActionState {
  shareHolders: ShareHolder[];
}

export interface ShareHolder {
  firstName: string;
  lastName: string;
  email: string;
  id: number;
  percentHolder?: number;
  isDirector?: boolean;
}
