import React, { useState } from "react";
import { ShareHolder } from "../../../redux//share-holder/state";
import ButtonComponent from "../../layout/button";
import InputComponent from "../../layout/input";

export interface ShareHolderEditComponentProps {
  sharer: ShareHolder;
  onUpdateSharer: (sharer: ShareHolder) => void;
  lastSharer?: boolean;
  onAddNewSharer: () => void;
}

const ShareHolderEditComponent = ({
  sharer,
  onUpdateSharer,
  lastSharer,
  onAddNewSharer
}: ShareHolderEditComponentProps) => {
  const [sharerEdit, setSharerEdit] = useState(sharer);

  const onChangeFirstName = (value: string) => {
    setSharerEdit({ ...sharerEdit, firstName: value });
    onUpdateSharer({ ...sharerEdit, firstName: value });
  };

  const onChangeLastName = (value: string) => {
    setSharerEdit({ ...sharerEdit, lastName: value });
    onUpdateSharer({ ...sharerEdit, lastName: value });
  };

  const onChangeEmail = (value: string) => {
    setSharerEdit({ ...sharerEdit, email: value });
    onUpdateSharer({ ...sharerEdit, email: value });
  };

  const checkUpdateShare = () => {
    onAddNewSharer();
  };

  return (
    <div className="box-form">
      <p className="form-title">Add a Shareholder</p>
      <div className="row-field">
        <InputComponent
          key="first-name"
          type="text"
          data-id="firstName.input"
          placeholderValue="Enter first name"
          labelValue="shareholders first name"
          value={sharerEdit.firstName}
          onChangeValue={onChangeFirstName}
        />
        <InputComponent
          key="last-name"
          type="text"
          data-id="lastName.input"
          placeholderValue="Enter last name"
          labelValue="shareholders last name"
          value={sharerEdit.lastName}
          onChangeValue={onChangeLastName}
        />
      </div>
      <div className="row-field">
        <InputComponent
          key="email-name"
          type="email"
          data-id="email.input"
          placeholderValue="Enter email"
          labelValue="Email"
          value={sharerEdit.email}
          onChangeValue={onChangeEmail}
        />
        <div className="form-group flex-row-center">
          {lastSharer && (
            <ButtonComponent
              className="btn btn-gray btn-add"
              title=""
              onClick={() => {
                checkUpdateShare();
              }}
            >
              {" "}
              <i className="fa fa-plus"></i> Add shareholder
            </ButtonComponent>
          )}
        </div>
      </div>
    </div>
  );
};

export default ShareHolderEditComponent;
