import React from "react";
import ShareHolderEditComponent, { ShareHolderEditComponentProps } from ".";
import { ReactWrapper, mount } from "enzyme";

const props: ShareHolderEditComponentProps = {
  sharer: {
    lastName: "Test Last Name",
    firstName: "Test First Name",
    percentHolder: 20,
    email: "Test email",
    id: 1
  },
  onUpdateSharer: jest.fn(),
  onAddNewSharer: jest.fn()
};

describe("ShareHolderEditComponent", () => {
  let component: ReactWrapper<ShareHolderEditComponentProps>;
  beforeEach(() => {
    component = mount(<ShareHolderEditComponent {...props} />);
  });
  it("should match snapshot", () => {
    expect(component).toMatchSnapshot();
  });
  it("renders input component", () => {
    expect(component.find("InputComponent")).toHaveLength(3);
  });
});
