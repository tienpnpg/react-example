import { ShareHolder } from "../../../redux/share-holder/state";
import React from "react";

export interface ShareHolderItemPercentComponentProps {
  isDirector?: boolean;
  sharer: ShareHolder;
  onChangePercentValue: (value: number) => void;
  onChangePositionValue: (value: string) => void;
  errPercentId: number | null;
}

const ShareHolderItemPercentComponent = ({
  isDirector,
  sharer,
  onChangePercentValue,
  errPercentId,
  onChangePositionValue
}: ShareHolderItemPercentComponentProps) => {
  const onChangePercent = (evt: React.ChangeEvent<HTMLInputElement>) => {
    if (Number.isInteger(parseInt(evt.target.value))) {
      onChangePercentValue(parseInt(evt.target.value));
    }
  };

  const onChangePosition = () => {
    onChangePositionValue(isDirector ? "NO" : "YES");
  };

  return (
    <div className={`item-details ${isDirector ? "active" : "child"}`}>
      <div className="details">
        <div className="col-first">
          <div>
            <p className="name">{`${sharer.firstName} ${sharer.lastName}`}</p>
            <p className="email">{sharer.email}</p>
          </div>
        </div>
        <div className="col-last">
          <input
            type="number"
            className={`form-control ${isDirector && "active"}`}
            value={sharer.percentHolder}
            onChange={onChangePercent}
          />
        </div>
        <div className="col-last">
          <button
            data-id="input-director"
            className={`form-control ${isDirector && "active"}`}
            onClick={onChangePosition}
          >
            {isDirector ? "YES" : "NO"}
          </button>
        </div>
      </div>
      {errPercentId !== null && errPercentId === sharer.id && (
        <p className="text-error">
          In total shareholders can only have a percentage that adds up to 100%, please review these details
        </p>
      )}
      <div className="border-bottom"></div>
    </div>
  );
};

export default ShareHolderItemPercentComponent;
