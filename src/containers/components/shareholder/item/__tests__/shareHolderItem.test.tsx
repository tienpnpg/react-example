import React from "react";
import ShareHolderItemPercentComponent, { ShareHolderItemPercentComponentProps } from "..";
import { ReactWrapper, mount } from "enzyme";

const props: ShareHolderItemPercentComponentProps = {
  sharer: {
    lastName: "Test Last Name",
    firstName: "Test First Name",
    percentHolder: 20,
    email: "Test email",
    id: 1
  },
  isDirector: true,
  onChangePercentValue: jest.fn(),
  onChangePositionValue: jest.fn(),
  errPercentId: 1
};

describe("ShareHolderItemPercentComponent", () => {
  let component: ReactWrapper<ShareHolderItemPercentComponentProps>;
  beforeEach(() => {
    component = mount(<ShareHolderItemPercentComponent {...props} />);
  });

  it("should match snapshot", () => {
    expect(component).toMatchSnapshot();
  });

  it("change value director", () => {
    component.find("button[data-id='input-director']").simulate("click");
    expect(component.find("button[data-id='input-director']").text()).toEqual('YES');
  });
});
