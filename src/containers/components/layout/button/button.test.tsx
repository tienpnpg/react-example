import React from "react";
import { shallow } from "enzyme";
import ButtonComponent from "./index";

describe("Button", () => {
  it('should render correctly in "debug" mode', () => {
    const clickFn = jest.fn();
    const component = shallow(
      <ButtonComponent className="btn btn-outline btn-primary mr-20" title="Back" onClick={clickFn} />
    );
    expect(component).toMatchSnapshot();
  });
});


