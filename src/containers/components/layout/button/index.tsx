import React from "react";
import "./styles.scss";

interface ButtonProps {
  className: string;
  onClick: () => void;
  title: string;
  children?: any;
}

const ButtonComponent = ({ onClick, className, title, children }: ButtonProps) => {
  return (
    <button className={className} onClick={onClick}>
      {title}
      {children}
    </button>
  );
};

export default ButtonComponent;
