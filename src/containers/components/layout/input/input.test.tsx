import React from "react";
import { ReactWrapper, mount } from "enzyme";
import InputComponent, { InputProps } from ".";

const props: InputProps = {
  value: "Test Input",
  className: "form-group form-vertical",
  onChangeValue: jest.fn(e => e),
  type: "text",
  placeholderValue: "Test place holder input"
};

describe("InputComponent", () => {
  let component: ReactWrapper<InputProps>;
  const valueInputSimulate: string = "valueInputSimulate";
  beforeEach(() => {
    component = mount(<InputComponent {...props} />);
  });
  it("match snapshot", () => {
    expect(component).toMatchSnapshot();
  });

  it("render correct input", () => {
    expect(component.find("input")).toBeTruthy();
    expect(component.props().value).toBe(props.value);
  });

  it("on change value input", () => {
    expect(component.find("input").simulate("change", { target: { value: valueInputSimulate } }));
    expect(component.props().onChangeValue).toBeCalled();
  });

  it("render failed input", () => {
    expect(component.find("input").simulate("change", { target: { value: "" } }));
    expect(component.props().onChangeValue).toBeCalled();
    setTimeout(() => {
      expect(component.props().value).toBe("");
      expect(component.contains(<small className="error">Invalid</small>)).toEqual(true);
    }, 300);
  });
});
