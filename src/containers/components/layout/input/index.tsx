import React, { useState } from "react";
import "./styles.scss";
import { validateEmail } from "../../../utils";

export interface InputProps {
  value: string;
  labelValue?: string;
  onChangeValue: (value: string) => void;
  type: string;
  placeholderValue?: string;
  className?: string;
}

const InputComponent = ({ value, labelValue, onChangeValue, type, placeholderValue, className }: InputProps) => {
  const [edited, setEdited] = useState(false);
  const onChangeInput = (evt: React.ChangeEvent<HTMLInputElement>) => {
    !edited && setEdited(true);
    onChangeValue(evt.target.value);
  };

  const checkValidInput = () => {
    let validRegex = true;
    if (type === "email") {
      validRegex = validateEmail(value);
      return edited && !validRegex;
    }
    return !value.length && edited;
  };

  return (
    <div className={className ? "" : "form-group form-vertical"}>
      {labelValue && <label>{labelValue}</label>}
      <input
        type={type}
        className="form-control"
        placeholder={placeholderValue}
        value={value}
        onChange={onChangeInput}
      />
      {checkValidInput() && <small className="error">Invalid</small>}
    </div>
  );
};

export default InputComponent;
