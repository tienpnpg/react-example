import React from "react";
import { useHistory } from "react-router-dom";
import ButtonComponent from "../button";
import "./styles.scss";

const FooterComponent = () => {
    const history = useHistory();
    return (
        <footer className="footer">
            <div className="container  flex-row-end">
                <ButtonComponent
                    className="btn btn-outline btn-primary mr-20"
                    title="Back"
                    onClick={() => {
                        history.replace("/page1");
                    }}
                />
                <ButtonComponent
                    className="btn btn-gradient"
                    title="Next"
                    onClick={() => {
                        history.replace("/page2");
                    }}
                />
            </div>
        </footer>
    );
};

export default FooterComponent;
