import LoadingComponent from "../../loading";
import React from "react";
import HeaderComponent from "../header";
import { IProps } from "./propState";

export default function ContainerComponent(props: IProps) {
  return (
    <>
      <LoadingComponent />
      <HeaderComponent />
      <div className="container">
        <div>{props.children}</div>
      </div>
    </>
  );
}
