import React from "react";
import { ReactWrapper, mount } from "enzyme";
import HeaderComponent from "../";
import ButtonComponent from "../../button";

describe("Footer", () => {
  let component: ReactWrapper;
  beforeEach(() => {
    component = mount(<HeaderComponent />);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render correctly in "debug" mode', () => {
    expect(component).toMatchSnapshot();
  });

  it("open modal", () => {
    component
      .find("ButtonComponent")
      .first()
      .find("button")
      .simulate("click");
    setTimeout(() => {
      expect(
        component.contains(
          <ButtonComponent className="btn btn-outline btn-primary mr-10" title="Cancel" onClick={jest.fn()} />
        )
      ).toEqual(true);
    }, 300);
  });

  it("close modal", () => {
    component
      .find("ButtonComponent")
      .first()
      .find("button")
      .simulate("click");
    component
      .find("ModalComponent")
      .find("ButtonComponent")
      .first()
      .find("button")
      .first()
      .simulate("click");
    expect(
      component.contains(
        <ButtonComponent className="btn btn-outline btn-primary mr-10" title="Cancel" onClick={jest.fn()} />
      )
    ).toEqual(false);
  });
});
