import React, { useState } from "react";
import ButtonComponent from "../button";
import ModalComponent from "../modal";
import logo from "../../../../assets/images/logo.png";
import "./styles.scss";

const HeaderComponent = () => {
  const [modalIsOpen, setIsOpen] = useState(false);

  const openModal = () => {
    setIsOpen(true);
  };
  const closeModal = () => {
    setIsOpen(false);
  };
  return (
    <header className="header">
      <div className="container header-container">
        <div className="logo-container">
          <img src={logo} alt="" className="logo" />
        </div>
        <div className="header-item-wrap">
          <span className="header-item">What we do</span>
          <span className="header-item">Advantages</span>
          <span className="header-item">Pricing</span>
          <span className="header-item">How it works</span>
        </div>
        <ButtonComponent
          className="btn btn-clear btn-outline btn-light"
          title="Save and exit"
          data-id="btn-open-modal"
          onClick={() => {
            openModal();
          }}
        />
        <div className="dropdown">
          <button onClick={() => {}} className="dropbtn">
            Dropdown
          </button>
        </div>
      </div>
      <ModalComponent opened={modalIsOpen} onCloseModal={closeModal}>
        <div className="modal modal-details">
          <div className="modal-header">
            <p className="title">Confirm details</p>
            <i
              className="fa fa-close icon-close"
              onClick={() => {
                closeModal();
              }}
            ></i>
          </div>
          <div className="modal-body">
            <div>
              <div className="row-details">
                <p className="label">Account name:</p>
                <p className="text-field">Sir Ian McKellen</p>
              </div>
              <div className="row-details">
                <p className="label">Sort Code:</p>
                <p className="text-field">00-00-00</p>
              </div>
              <div className="row-details">
                <p className="label">Account number:</p>
                <p className="text-field">234234234</p>
              </div>
              <div className="row-details">
                <p className="label">Amount:</p>
                <p className="text-field">£3,500.00</p>
              </div>
              <div className="row-details">
                <p className="label">Reference:</p>
                <p className="text-field">sdwer23423423</p>
              </div>
            </div>
            <p className="text-details">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vitae mauris eu nibh sagittis lobortis at id
              tortor. Sed vitae diam vel sapien interdum tincidunt. Vivamus ligula lectus, faucibus a lectus dapibus,
              vehicula vulputate sapien. Quisque blandit, lectus interdum mollis feugiat, turpis dolor venenatis elit,
              nec pellentesque odio enim nec eros.
            </p>
          </div>
          <div className="modal-footer">
            <ButtonComponent
              className="btn btn-outline btn-primary mr-10"
              title="Cancel"
              onClick={() => {
                closeModal();
              }}
            />
            <ButtonComponent
              className="btn btn-primary"
              title="Confirm details"
              onClick={() => {
                closeModal();
              }}
            />
          </div>
        </div>
      </ModalComponent>
    </header>
  );
};

export default HeaderComponent;
