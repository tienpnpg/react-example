import React from "react";
import Modal from "react-modal";
import "./style.scss";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: "auto"
  }
};

if (process.env.NODE_ENV !== "test") Modal.setAppElement("#getground-root");

const ModalComponent = ({ opened, children, onCloseModal }) => {
  const afterOpenModal = () => {};

  const closeModal = () => {
    onCloseModal();
  };
  return (
    <Modal isOpen={opened} onAfterOpen={afterOpenModal} onRequestClose={closeModal} style={customStyles}>
      {children}
    </Modal>
  );
};

export default ModalComponent;
