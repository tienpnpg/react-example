import React from "react";
import { mount } from "enzyme";
import ModalComponent from "../";

const props = {
  opened: true,
  children: <></>,
  onCloseModal: jest.fn()
};

describe("ModalComponent", () => {
  it('should render correctly in "debug" mode', () => {
    const component = mount(<ModalComponent {...props} />);
    expect(component).toMatchSnapshot();
  });
});
