import { RouterState } from "connected-react-router";
import IShareHoldersActionState from "../containers/redux/share-holder/state";
import CommonActionState from "../containers/redux/state";

export interface RootState {
  router: RouterState;
  common: CommonActionState;
  shareHolders: IShareHoldersActionState;
}
