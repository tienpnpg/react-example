import { connectRouter } from "connected-react-router";
import commonReducer from "../containers/redux/reducers";
import shareHolderReducer from "../containers/redux/share-holder/reducers";
import { History } from "history";
import { combineReducers } from "redux";

export default (history: History) =>
  combineReducers({
    router: connectRouter(history),
    common: commonReducer,
    shareHolders: shareHolderReducer
  });
