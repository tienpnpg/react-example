import { configure } from "enzyme";
import ReactSixteenAdapter from "enzyme-adapter-react-16";

jest.mock("react-redux", () => ({
  useSelector: jest.fn().mockReturnValue({
    shareHolders: [
      {
        id: 0,
        email: "mememememe@gmail.com",
        firstName: "you",
        lastName: "",
        percentHolder: 100
      }
    ]
  }),
  useDispatch: () => jest.fn()
}));

jest.mock("react-router-dom", () => ({
  useHistory: () => ({
    push: jest.fn(),
    replace: jest.fn()
  })
}));

configure({ adapter: new ReactSixteenAdapter() });
