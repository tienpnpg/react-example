const path = require('path');

module.exports = {
  process(src, filename) {
    return `module.exports = {
							id: '${path.basename(filename).split('.')[0]}',
							symbol: '${path.basename(filename)}',
							viewBox: 'string',
							content: 'string',
					};`;
  },
};
